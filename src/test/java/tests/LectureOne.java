package tests;

import org.testng.annotations.*;

public class LectureOne {

    @Test
    public void nameToCharsToASCII() {

        String myName = "Alexander";
        int currentASCII = 0;
        char[] chArray = myName.toCharArray();
        for (int i = 0;
                i < chArray.length; i++) {
            currentASCII = (int) (chArray[i]);
            System.out.print(currentASCII + " ");

        }
    }
    @Test
    public void nameToCharsToUnicode() {
        String myName = "Alexander";
        char[] chArray = myName.toCharArray();
        for (int i = 0;
             i < chArray.length; i++) {
            System.out.print(toUnicode(chArray[i]));

        }

    }
    private static String toUnicode(char ch) {
        return String.format("\\u%04x", (int) ch);
    }
    @Test
    public  void printText(){

        String text = "\\’Hello’\\ to\\\\\\ \"World\" \\’";
        System.out.print(text);
    }

    @Test
    public void optimalVar(){

        short number = 32767;
        System.out.print(number);
    }
    @Test
    public void aritmeticOperations(){

        int a = 2;
        int b = 3;
        int c = 5;
        int d = 10;

       boolean canDivide = (d % a == 0); // if firstnumber can be devided exactly by second number boolean will be true
        //System.out.print(d%a + "\n");
        //System.out.print(++a + "\n"); // increases number by 1
        //System.out.print(++a + "\n"); // decreases number by 1
        //System.out.print(-a + "\n"); // changes positive number to negative and negative to positive
        //System.out.print(canDivide + "\n"); //prints true value of boolean
        //System.out.print(!canDivide + "\n");//prints opposite value of boolean


    }

}

